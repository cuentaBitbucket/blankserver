var helper = require('../general/helper');
import { UserManager } from "../managers/userManager";
import { User } from '../models/User';

export class UserService {
    constructor(app, MongoClient, url, dbName) {
        const userManager = new UserManager(MongoClient, url, dbName);

        app.get("/users", function (req, res) {
            const users = userManager.getList().map(user => {
                delete user.pass;
                return user;
            })
            res.status(200).send(users);
        });

        app.get("/users/:id", function (req, res) {
            const id = req.params.id;
            const user = { ...userManager.getUserById(id) };
            if (Object.keys(user).length === 0 && user.constructor === Object)
                return res.status(404).send("Usuario no existe");
            delete user.pass;
            res.status(200).send(user);
        });

        app.post("/users", function (req, res) {
            var user = req.body;
            user.pass = helper.hashPassword(user.pass);
            userManager.addUserToDB(user, res);
        });

        app.delete("/users/:id", function (req, res) {
            const id = req.params.id;
            const user = userManager.deleteUser(id);
            if (user == null)
                return res.status(404).send("Usuario no existe");
            delete user.pass;
            res.status(200).send(user);
        });

        app.get("/login", function (req, res) {
            var session = req.body;
            const pass = session.pass;
            const username = session.username;

            if (pass == null || username == null)
                return res.status(401).send("Datos erroneos");

            var user: User = userManager.loginUser(username, helper.hashPassword(pass));
            if (user == null) {
                res.status(401).send("Usuario incorrecto");
                return;
            }

            console.log("-" + user.username + " [" + user.mail + "] logged");
            delete user.pass;
            res.status(200).send(user);
        });
    }
}