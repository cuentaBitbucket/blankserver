import { UserService } from "../services/userServices";

export class Services {
    constructor(app, MongoClient, url, dbName) {
        new UserService(app, MongoClient, url, dbName);
    }
}