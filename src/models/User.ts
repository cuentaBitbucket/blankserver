export class User{	
    public username: string;
    public mail: string;
    public pass: string;
    public _id: string;

	constructor(username, mail, pass){
		this.username = username;
		this.mail = mail;
        this.pass = pass;
        this._id = null;
	}
	
	public fill(user){
		var list = Object.entries(user);
		for(var i=0; i<list.length; i++){
			var atr = list[i];
			if(!atr[1])
				continue;
			if(atr[0])
				this[atr[0]] = atr[1];
		}
	}
	
	public getData(){
		return{
			username: this.username,
			mail: this.mail,
			pass: this.pass,
			_id: this._id
		}
	}
};