const app = require('express')();
const http = require('http').Server(app);
const bodyParser = require('body-parser');
const cors = require('cors');

const MongoClient = require('mongodb').MongoClient;
const url = "mongodb://localhost:27017/";
const dbName = "ddbb";
const port:number = 9525;

import {Services} from "./services/services";

init();

function init() {
	//Middleware stack
	app.use(bodyParser.json({ limit: '50mb' }));
	//to suport URL-encoded bodies
	app.use(bodyParser.urlencoded({ extended: true }));

	app.options('*', cors())
	app.use(cors());

	listen();

	new Services(app, MongoClient, url, dbName);
}

function listen() {
	http.listen(port, function () {
		console.log('BasicServer listening on port ' + port);
	});
}