import { Persistence } from "../general/persistence";
import { User } from "../models/User";
const helper = require("../general/helper");

export class UserManager {
	private persistence: Persistence;
	private collectionName: string;
	private users: Array<User>;

	constructor(MongoClient, url, dbName) {
		this.collectionName = "userList";
		this.users = [];
		this.persistence = new Persistence(MongoClient, url, dbName);
		this.persistence.getItemsFromDB(this.collectionName, (data) => { this.getUsers(data) });
	}

	/*===== GENERAL =====*/
	public getUsers(data) {
		this.users = data;
	}

	public getList() {
		return this.users;
	}

	public addUserToDB(userData, res) {
		console.log("Users: ", this.users.length);
		const user = new User(userData.username, userData.mail, userData.pass);
		this.persistence.addToDB(this.collectionName, user.getData(),
			(user) => {
				this.users.push(user);

				const userSend = JSON.parse(JSON.stringify(user));
				delete userSend.pass;
				res.status(200).send(userSend);
			}, (item) => res.status(200).send(item), res);
	}

	public deleteUser(id): User {
		const user = this.users.find(user => user._id == id);
		if (user == undefined) return null;

		helper.delFromList(this.users, id);
		this.persistence.deleteFromBD(this.collectionName, id);
		return JSON.parse(JSON.stringify(user));
	}

	/*===== OTHERS =====*/
	public loginUser(name, pass): User {
		const user = this.users.find(user => (user.pass == pass && name == user.username));
		if (user == null)
			return null;

		return JSON.parse(JSON.stringify(user));
	}

	public getUserById(id: string) {
		return this.users.find(user => user._id == id);
	}
}