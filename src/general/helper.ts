this.crypto = require('crypto');

this.exist = function (name, items) {
	for (var i = 0; i < items.length; i++)
		if (name == items[i].userName)
			return true;
	return false;
}

this.getItem = function (name, items) {
	if(name == null) return null;
	
	for (var i = 0; i < items.length; i++) {
		if (name.toLowerCase() == items[i].userName.toLowerCase())
			return items[i];
	}
	return null;
}

this.getItemByMail = function (mail, items) {
	for (var i = 0; i < items.length; i++) {
		if (mail == items[i].mail)
			return items[i];
	}
	return null;
}

this.getIdById = function (id, items) {
	for (var i = 0; i < items.length; i++) {
		if (id == items[i]._id)
			return i;
	}
	return null;
}

this.getItemById = function (id, items) {
	for (var i = 0; i < items.length; i++)
		if (id + "" == items[i]._id + "")
			return items[i];
	return null;
}

this.getItemsByParam = function (id, items, param) {
	var list = [];
	for (var i = 0; i < items.length; i++) {
		if (id == items[i][param])
			list.push(items[i]);
	}
	return list;
}

this.getIdByName = function (name, items) {
	for (var i = 0; i < items.length; i++)
		if (name == items[i].name)
			return items[i]._id;
	return null;
}

this.delFromList = function (list, data) {
	if (this.getIdById(data, list) == null)
		return;

	list.splice(this.getIdById(data, list), 1);
}

this.delFromListWithI = function (list, i) {
	list.splice(i, 1);
}

this.delFromSimpleList = function (list, data) {
	var newList = [];
	for (var i = 0; i < list.length; i++) {
		if (list[i] != data) {
			newList.push(list[i]);
		}
	}

	return newList;
}

this.contains = function (list, id) {
	for (var i = 0; i < list.length; i++) {
		if (list[i].toString() == id.toString())
			return true;
	}
	return false;
}

//String a minusculas y sin tildes
this.tidyText = function (str) {
	str = str.toLowerCase();
	var diacritics = [
		{ char: 'A', base: /[\300-\306]/g },
		{ char: 'a', base: /[\340-\346]/g },
		{ char: 'E', base: /[\310-\313]/g },
		{ char: 'e', base: /[\350-\353]/g },
		{ char: 'I', base: /[\314-\317]/g },
		{ char: 'i', base: /[\354-\357]/g },
		{ char: 'O', base: /[\322-\330]/g },
		{ char: 'o', base: /[\362-\370]/g },
		{ char: 'U', base: /[\331-\334]/g },
		{ char: 'u', base: /[\371-\374]/g },
		{ char: 'N', base: /[\321]/g },
		{ char: 'n', base: /[\361]/g },
		{ char: 'C', base: /[\307]/g },
		{ char: 'c', base: /[\347]/g }
	]

	diacritics.forEach(function (letter) {
		str = str.replace(letter.base, letter.char);
	});

	return str;
};

this.hashPassword = function (key) {
	var text = 'KlaatuBaradaNikto';
	var cipher = this.crypto.createCipher('aes256', key);
	return cipher.update(text, 'utf8', 'hex') + cipher.final('hex');
}

this.createPsw = function () {
	var letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
	var numbers = '0123456789';
	var psw = "";
	for (var i = 0; i < 7; i++)
		psw += letters.charAt(Math.floor(Math.random() * letters.length));
	for (var i = 0; i < 3; i++)
		psw += numbers.charAt(Math.floor(Math.random() * numbers.length));

	return psw;
}

this.resOk = function (res, item) {
	res.json(item);
}

this.validateUserInputs = function (user) {
	if (user.name.length < 2)
		return 2;

	if (user.name.length >= 20)
		return 3;

	if (user.surname.length < 2)
		return 4;

	if (user.surname.length >= 20)
		return 5;

	var mail = user.mail.split("@");

	if (mail[0] != "") {
		if (mail.length == 1)
			return 6;

		var domain = mail[1].split(".");

		if (domain.length < 2)
			return 6;

		if (mail[0].length == 0 || domain[0].length == 0 || domain[1].length == 0)
			return 6;
	}
	else {
		return 6;
	}

	return -1;
}

this.createNewId = function (list) {
	return list.reduce((acc, item) => item._id >= acc ? item._id : acc, 0) + 1;
}


this.isNotAuthorizedToModify = function (sessionId, sessionRol, userId ,userRol) {
	//Si es admin pot fer tot
	if (sessionRol === "admin")
		return false;

	//Si s'autoritza modificar a altres
	if (userId !== undefined) {
		//Si es el mateix usuari
		if (sessionId === userId)
			return false;

		//Si es admin pot fer tot menys a un lider
		if (sessionRol === "lider" && userRol !== "admin")
			return false;

		//Si no ha de modificar cap usuari
	} else if (sessionRol === "lider")
		return false;

	return true;
}


const algorithm = 'aes-256-cfb';
const psw = "d6F3Efeq";

this.encryptData = function(data, secret){
	if(!data)
		return null;
		
	if(!secret)
		secret = psw;

	secret = secret+"";

	var cipher = this.crypto.createCipher(algorithm, secret)
	var crypted = cipher.update(data,'utf8','hex')
	crypted += cipher.final('hex');
	return crypted;
}

this.decryptData = function(encryptedData, secret){
	if(!encryptedData)
		return null;

	if(!secret)
		secret = psw;

	secret = secret+"";

	var decipher = this.crypto.createDecipher(algorithm, secret)
	var dec = decipher.update(encryptedData,'hex','utf8')
	dec += decipher.final('utf8');

	return dec;
}

this.getDateNow = function(d) {

	d = d == null ? new Date() : d;

	var day = d.getDate();
	var month = d.getMonth() + 1;
	var year = d.getFullYear();
	var hours = d.getHours();

	if (day.toString().length == 1)
		day = "0" + day;
	if (month.toString().length == 1)
		month = "0" + month;

	return day + "-" + month + "-" + year + "-" + hours;
}