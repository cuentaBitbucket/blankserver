var ObjectID = require('mongodb').ObjectID;

export class Persistence {
	private MongoClient;
	private url: string;
	private dbName: string;

	constructor(mongoClient, url, dbName) {
		this.MongoClient = mongoClient;
		this.url = url;
		this.dbName = dbName;
	}

	//Agafar els elements de la base de dades
	public getItemsFromDB(dbListName, callback) {
		console.log(this.dbName);
		this.MongoClient.connect(this.url, { useNewUrlParser: true }, (err, db) => {
			if (err) {
				console.log(err);
				return;
			}
			var dbo = db.db(this.dbName);
			dbo.collection(dbListName).find({}).toArray((err, items) => {
				if (err) throw err;
				if (items.length == 0) {
					console.log("no " + dbListName + " in db");
					return [];
				}
				console.log("Getting " + dbListName + ": " + items.length);
				db.close();
				callback(items);
			});
		});

	}

	/*	Afegir un element a la base de dades
		listName: nom de la llista,
		item: objecte que es vol afegir
		callback: Primer callback. Normalment per afegir l'item a la llista del manager amb la _id del mongo
		callback2: Funció que es crida en el primer callback. Normalment funció que defineix el que es retorna al client
		response: Resposta http
	*/
	public addToDB(listName, item, callback, callback2, response) {
		if (item == undefined) {
			console.log("item undefined")
			return;
		}
		this.MongoClient.connect(this.url, (err, db) => {
			var dbo = db.db(this.dbName);
			dbo.collection(listName).insertOne(item, (err, res) => {
				if (err) throw err;
				console.log("-Adding " + item.username + " to " + listName);
				db.close();
				if (callback != undefined)
					callback(item, callback2, response);
			});
		});
	}

	public deleteFromBD(listName, id) {
		this.MongoClient.connect(this.url, (err, db) => {
			if (err) throw err;
			var dbo = db.db(this.dbName);
			var myquery = { _id: new ObjectID(id) };
			dbo.collection(listName).deleteOne(myquery, (err, obj) => {
				if (err) throw err;
				console.log("delete from " + listName);
				db.close();
			})
		});
	}

	//Elimina una llista de la bbdd i l'omple amb el coningut de list
	public newList(listName, list) {
		this.MongoClient.connect(this.url, (err, db) => {
			var dbo = db.db(this.dbName);
			dbo.collection(listName).drop((err, delOK) => {
				if (err) {
					console.log(listName + " does not exist");
					return;
				}
				if (delOK) {
					if (list == null)
						console.log(listName + " deleted");
					else
						console.log(listName + " updated");
				}
				db.close();
			});

			if (list == null) return;
			dbo.collection(listName).insertMany(list, (err, res) => {
				if (err) throw err;
				db.close();
			});
		});
	}

	//Modifica una variable d'un objecte
	public updateItem(id, newValues, listName, callback, response) {
		this.MongoClient.connect(this.url, (err, db) =>{
			if (err) throw err;
			var dbo = db.db(this.dbName);
			var myquery = { _id: id };

			dbo.collection(listName).updateOne(myquery, newValues, (err, res) => {
				if (err) throw err;
				console.log(listName + " updated");
				db.close();
				if (callback != undefined)
					callback(response, true);
			});
		});
	}
}